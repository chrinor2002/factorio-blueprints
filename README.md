# Example Terminal Commands

Takes from a file, into the clipboard
```
cat 0.16/ltn/LTN\ Nuclear\ Fuel.book.json | ./bin/encode.sh | pbcopy
```

Takes from the clipboard, into a file
```
bppaste | ./bin/decode.sh > 0.16/ltn/temp
```
