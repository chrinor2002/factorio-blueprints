#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

"$DIR/../../../bin/merge.js" \
  -i 0 -y -2 \
  -i 0 -y 5 \
  -i 1 -y 5 \
  -i 7 -y 0 \
  -i 14 -y 4 \
  -i 22 -y 6 \
  -i 27 -y 4 \
  -i 28 -y -2 \
  "$DIR/../../factories/Steam Power.book.json"