#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

"$DIR/../../../bin/merge.js" \
  -i 2 -y -5 \
  -i 10 -y 4 \
  -i 11 \
  "$DIR/../../factories/Wood Farm.book.json"