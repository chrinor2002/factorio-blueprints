#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

"$DIR/../../../bin/merge.js" \
  -i 0 \
  -i 4 \
  -i 5 \
  -i 14 \
  "$DIR/../../factories/City Blocks.book.json"