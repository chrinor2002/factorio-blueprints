#!/usr/bin/env node

const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const glob = require('glob');
const child_process = require('child_process');
const {promisify} = require('util');
const statAsync = promisify(fs.stat);
const globAsync = promisify(glob);

const rootPath = path.join(__dirname, '/../');
console.log('root', rootPath);

const globAsyncCache = (...args) => {
    return new Promise((resolve, reject) => {
        const mg = glob(...args, (err, files) => {
            err ? reject(err) : resolve(mg.cache);
        });
    });
}
const handleDirChange = (watcher, sourceDir, eventType, filename) => {
    console.log(sourceDir, 'changed with a', eventType);
    globAsync(sourceDir + '/**/*.blueprint').then((files) => {
        if (files.length > 0) {
            console.log('found blueprints');
            _.map(files, processBlueprint)
        }
    });
};
const processBlueprint = (file) => {
    console.log('processing', file);
    // get decoded string
    const decoded = child_process.execSync(`cat "${file}" | bin/decode.sh`);
    // remove the file (so we dont process it again
    fs.unlinkSync(file);
    // save the decoded
    fs.writeFileSync(`${file}.json`, decoded);
};

globAsyncCache('!(node_modules|bin)/**/*').then((files) => {
    const dirs = _(files).toPairs().filter((pair) => {
        const [filePath, cacheValue] = pair;
        return cacheValue === 'DIR' || _.isArray(cacheValue);
    }).fromPairs().keys().value();

    _.each(dirs, (dir) => {
        console.log('watching', dir);
        const watcher = fs.watch(dir);
        watcher.on('change', (...args) => {
            handleDirChange(watcher, dir, ...args)
        });
        handleDirChange(watcher, dir, 'startup', dir)
    });
})
