#!/bin/bash

function checkInstalled() {
  if [ "$(which $1)" = "" ]; then
    echo "$1 not found, is it installed?"
  fi
}

checkInstalled pigz
checkInstalled jq

dd if=/dev/stdin bs=1 skip=1 | base64 --decode | pigz -d | jq .
