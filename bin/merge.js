#!/usr/bin/env node

const _ = require('lodash');
const path = require('path');
const fs = require('fs');

const argz = process.argv.slice(2);
let inputConfig = [];
let file = null;
let currentIndexArg = null;
let argIndex = 2;
while(argz.length > 0) {
    let arg = argz.shift();
    argIndex++;
    switch(arg) {
        case '-i':
            // TODO: range error check?
            currentIndexArg = {
                i: parseInt(argz.shift())
            };
            inputConfig.push(currentIndexArg);
            argIndex++;
            break;

        case '-x':
            if (!currentIndexArg) {
                process.stderr.write(`cant put ${arg} at argument index ${argIndex}\n`);
                break;
            }
            currentIndexArg.x = parseInt(argz.shift());
            argIndex++;
            break;

        case '-y':
            if (!currentIndexArg) {
                process.stderr.write(`cant put ${arg} at argument index ${argIndex}\n`);
                break;
            }
            currentIndexArg.y = parseInt(argz.shift());
            argIndex++;
            break;

        default:
            if (argz.length > 1) {
                process.stderr.write(`unkown argument "${arg}"`);
            }
            file = arg;
    }
}

const applyOffset = (original, offset) => {
    return original + offset;
};
const applyXYOffsetToItem = (entity, conf) => {
    if (conf.x) {
        entity.position.x = applyOffset(entity.position.x, conf.x)
    }
    if (conf.y) {
        entity.position.y = applyOffset(entity.position.y, conf.y)
    }
};
const applyXYOffsetToBlueprint = (blueprint, conf) => {
    if (blueprint.entities) {
        blueprint.entities.forEach((v) => applyXYOffsetToItem(v, conf));
    }
    if (blueprint.tiles) {
        blueprint.tiles.forEach((v) => applyXYOffsetToItem(v, conf));
    }
};

const data = JSON.parse(fs.readFileSync(file, 'utf8'));
const book = data.blueprint_book;

const result = inputConfig.reduce((accu, conf) => {
    const { i } = conf;
    const toAdd = _.cloneDeep(book.blueprints[i].blueprint);
    if (!toAdd) {
        throw new Error(`no blueprint for index ${i}`);
    }
    process.stderr.write(`stacking ${toAdd.label}...\n`);

    process.stderr.write(`applying offset(${conf.x || 0}, ${conf.y || 0}) to ${i} (${toAdd.label})\n`);
    applyXYOffsetToBlueprint(toAdd, conf);

    if (accu.icons.length < 4) {
        accu.icons.push(toAdd.icons[0]);
    }
    if (toAdd.entities) {
        toAdd.entities.forEach((v) => accu.entities.push(v));
    }
    if (toAdd.tiles) {
        toAdd.tiles.forEach((v) => accu.tiles.push(v));
    }
    return accu;
}, {
    label: book.label,
    icons: [],
    entities: [],
    tiles: [],
    item: 'blueprint',
    version: book.version
});

// Re-index entities
let i = 1;
result.entities.forEach((entity) => {
    entity.entity_number = i++;
});
const final = {
    blueprint: result
}
process.stdout.write(JSON.stringify(final, null, 2));