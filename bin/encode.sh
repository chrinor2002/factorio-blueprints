#!/bin/bash

function checkInstalled() {
  if [ "$(which $1)" = "" ]; then
    echo "$1 not found, is it installed?"
  fi
}

checkInstalled pigz
checkInstalled jq

VERSION = $1

case $space in
0.15)
0.16)
  printf "\x30"
  ;;
1.00)
  echo "1.00 not supported yet, check the script you sillybilly." > 2
  exit 1
  #printf "\x32"
  ;;
1.00)
  echo "1.00 not supported yet, check the script you sillybilly." > 2
  exit 1
  ;;
0.17)
*)
  printf "\x30"
  ;;
esac

cat /dev/stdin | jq . -c | pigz -c -z -8 | base64