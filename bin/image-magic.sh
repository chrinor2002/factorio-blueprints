#!/bin/bash

DATA_DIR="/Users/chrisr/Library/Application Support/Steam/steamapps/common/Factorio/factorio.app/Contents/data"

PID=$$

docker pull v4tech/imagemagick
docker run --rm \
  -v "$DATA_DIR:/tmp/$PID/data" \
  -v "$PWD:/tmp/$PID/images" \
  -w /tmp/$PID/images \
  v4tech/imagemagick \
  convert ../data/base/graphics/entity/player/hr-level1_running.png ./hr-level1_running.jpg