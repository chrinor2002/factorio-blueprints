#!/bin/bash

function checkInstalled() {
  if [ "$(which $1)" = "" ]; then
    echo "$1 not found, is it installed?"
  fi
}

checkInstalled gzip

cat /dev/stdin | gzip -8 | base64