#!/usr/bin/env node

const _ = require('lodash');
const path = require('path');
const fs = require('fs');

// 7  0  1
// 6     2
// 5  4  3
const DIRECTION_MIRROR_X = new Map();
DIRECTION_MIRROR_X.set(undefined, 0);
DIRECTION_MIRROR_X.set(null, 0);
DIRECTION_MIRROR_X.set(0, 0);
DIRECTION_MIRROR_X.set(1, 7);
DIRECTION_MIRROR_X.set(2, 6);
DIRECTION_MIRROR_X.set(3, 5);
DIRECTION_MIRROR_X.set(4, 4);
DIRECTION_MIRROR_X.set(5, 3);
DIRECTION_MIRROR_X.set(6, 2);
DIRECTION_MIRROR_X.set(7, 1);

const DIRECTION_MIRROR_Y = new Map();
DIRECTION_MIRROR_Y.set(undefined, 4);
DIRECTION_MIRROR_Y.set(null, 4);
DIRECTION_MIRROR_Y.set(0, 4);
DIRECTION_MIRROR_Y.set(1, 3);
DIRECTION_MIRROR_Y.set(2, 2);
DIRECTION_MIRROR_Y.set(3, 1);
DIRECTION_MIRROR_Y.set(4, 0);
DIRECTION_MIRROR_Y.set(5, 7);
DIRECTION_MIRROR_Y.set(6, 6);
DIRECTION_MIRROR_Y.set(7, 5);

const DIRECTION_MIRROR_XY = new Map();
DIRECTION_MIRROR_XY.set(undefined, 4);
DIRECTION_MIRROR_XY.set(null, 4);
DIRECTION_MIRROR_XY.set(0, 4);
DIRECTION_MIRROR_XY.set(1, 5);
DIRECTION_MIRROR_XY.set(2, 6);
DIRECTION_MIRROR_XY.set(3, 7);
DIRECTION_MIRROR_XY.set(4, 0);
DIRECTION_MIRROR_XY.set(5, 1);
DIRECTION_MIRROR_XY.set(6, 2);
DIRECTION_MIRROR_XY.set(7, 3);

const argz = process.argv.slice(2);
let file = null;
let mirrorX = false;
let mirrorY = false;
let argIndex = 2;
while(argz.length > 0) {
    let arg = argz.shift();
    argIndex++;
    switch(arg) {

        case '-x':
            mirrorX = true;
            argIndex++;
            break;

        case '-y':
            mirrorY = true;
            argIndex++;
            break;

        default:
            if (argz.length > 1) {
                process.stderr.write(`unkown argument "${arg}"`);
            }
            file = arg;
    }
}

const mirrorToNumber = (original) => {
    return original * -1;
};
const applyMirrorToItem = (entity) => {
    const originalPosition = _.clone(entity.position);
    if (mirrorX) {
        _.each(entity, (v, k) => {
            if (_.has(v, 'x')) {
                v = _.get(v, 'x');
                _.set(entity, [k, 'x'], mirrorToNumber(v));
            }
        });
    }
    if (mirrorY) {
        _.each(entity, (v, k) => {
            if (_.has(v, 'y')) {
                v = _.get(v, 'y');
                _.set(entity, [k, 'y'], mirrorToNumber(v));
            }
        });
    }
    const originalDirection = _.clone(entity.direction);
    if (mirrorX && mirrorY) {
        entity.direction = DIRECTION_MIRROR_XY.get(entity.direction);
    }
    else if (mirrorX) {
        entity.direction = DIRECTION_MIRROR_X.get(entity.direction);
    }
    else if (mirrorY) {
        entity.direction = DIRECTION_MIRROR_Y.get(entity.direction);
    }
    process.stderr.write(
        `mirror ${entity.name} ` +
        `(d:${originalDirection}, ${originalPosition.x}, ${originalPosition.y})` +
        ` -> ` +
        `(d:${entity.direction}, ${entity.position.x}, ${entity.position.y})` +
        `...\n`);
};
const applyMirrorToBlueprint = (blueprint) => {
    if (blueprint.entities) {
        blueprint.entities.forEach(applyMirrorToItem);
    }
    if (blueprint.tiles) {
        blueprint.tiles.forEach(applyMirrorToItem);
    }
};
const applyMirrorToBook = (book) => {
    if (book.blueprints) {
        book.blueprints.forEach(applyMirrorToBlueprint);
    }
};

const applyMirrorToUnkown = (unkownEntity) => {
    if (unkownEntity.blueprint_book) {
        applyMirrorToBook(unkownEntity.blueprint_book);
    }
    if (unkownEntity.blueprint) {
        applyMirrorToBlueprint(unkownEntity.blueprint);
    }
};

if (file === '-' || !file) {
    file = 0;
}
const data = JSON.parse(fs.readFileSync(0, 'utf8'));
applyMirrorToUnkown(data);
process.stdout.write(JSON.stringify(data, null, 2));